package t1.dkhrunina.tm.util;

import java.text.DecimalFormat;

public interface FormatUtil {
    static String formatBytes(long bytes) {
        DecimalFormat decimalFormat = new DecimalFormat("#.###");

        final double kilobyte = 1024.0;
        final double megabyte = kilobyte * 1024;
        final double gigabyte = megabyte * 1024;
        final double terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return decimalFormat.format(bytes / kilobyte) + " KB";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return decimalFormat.format(bytes / megabyte) + " MB";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return decimalFormat.format(bytes / gigabyte) + " GB";
        } else if (bytes >= terabyte) {
            return decimalFormat.format(bytes / terabyte) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }
}